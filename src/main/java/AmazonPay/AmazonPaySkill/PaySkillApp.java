package AmazonPay.AmazonPaySkill;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.amazon.speech.speechlet.Directive;
import com.amazon.speech.speechlet.IntentRequest;
import com.amazon.speech.speechlet.LaunchRequest;
import com.amazon.speech.speechlet.LinkResultRequest;
import com.amazon.speech.speechlet.Session;
import com.amazon.speech.speechlet.SessionEndedRequest;
import com.amazon.speech.speechlet.SessionStartedRequest;
import com.amazon.speech.speechlet.Speechlet;
import com.amazon.speech.speechlet.SpeechletException;
import com.amazon.speech.speechlet.SpeechletResponse;
import com.amazon.speech.speechlet.StatusCode;
import com.amazon.speech.speechlet.interfaces.links.FollowLinkTarget;
import com.amazon.speech.speechlet.interfaces.links.directive.FollowLinkDirective;
import com.amazon.speech.ui.AskForPermissionsConsentCard;
import com.amazon.speech.ui.Image;
import com.amazon.speech.ui.PlainTextOutputSpeech;
import com.amazon.speech.ui.Reprompt;
import com.amazon.speech.ui.StandardCard;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.Sets;

public class PaySkillApp implements Speechlet {
	
	private ObjectMapper objectMapper = new ObjectMapper();
	private static String billingAgreementId = null;
	private static final String AMAZON_PAY_SCOPE = "payments:autopay_consent";
	private static final String REQUEST_AMAZON_PAY_PERMISSION_TITLE = "Needs Amazon Pay permission";
	public static final String PROCESS_PAYMENT_PATH = "processPayment";
	public static final String SETUP_PATH = "setup";

	public SpeechletResponse onIntent(IntentRequest request, Session session) throws SpeechletException {
		if (request.getIntent().getName().equals("AMAZON.HelpIntent")) {
			System.out.println("Help intent");
			return getHelp();
		}
		
		else if (request.getIntent().getName().equals("AMAZON.StopIntent")) {
			System.out.println("Stop intent");
			String stopAsk = "Stopped. What would you like to do with ticket buyer now?";
			return newAskResponse("Stopped Flow", stopAsk, stopAsk);
		}

		else if (request.getIntent().getName().equals("AMAZON.CancelIntent")) {
			System.out.println("Cancel intent");
			return tellRespose ("Goodbye", "Thank you and goodbye");

		}		

		else if (request.getIntent().getName().equals("tickets")){
	        SpeechletResponse speechletResponse = new SpeechletResponse();
	        List<Directive> directives = buildDirectives(session);
	        speechletResponse.setDirectives(directives);
	        speechletResponse.setShouldEndSession(true);
	        return speechletResponse; 
		}
		else if (request.getIntent().getName().equals("purchase")){
			SpeechletResponse speechletResponse = new SpeechletResponse();
			List<Directive> directivesPp = buildDirectivesProcessPayment(session);
			speechletResponse.setDirectives(directivesPp);
			speechletResponse.setShouldEndSession(true);
			return speechletResponse;
		}

		return null;
	}

	public SpeechletResponse onLaunch(LaunchRequest request, Session session) throws SpeechletException {
		// TODO Auto-generated method stub
		System.out.println("On Launch");
        String repromptText =
                "Welcome to beta movie ticket purchase skill. " +
                "To try the experience you can say tickets.";				
		return newAskResponse("Welcome to the ticket buyer", repromptText, repromptText);
	}

	public void onSessionEnded(SessionEndedRequest arg0, Session arg1) throws SpeechletException {
		// TODO Auto-generated method stub
		
	}

	public void onSessionStarted(SessionStartedRequest arg0, Session arg1) throws SpeechletException {
		// TODO Auto-generated method stub
		
	}
	private SpeechletResponse newAskResponse(String requestText, String respText, String repromptText) {
        PlainTextOutputSpeech outputSpeech = new PlainTextOutputSpeech();
        outputSpeech.setText(respText);
        PlainTextOutputSpeech repromptOutputSpeech = new PlainTextOutputSpeech();
        repromptOutputSpeech.setText(repromptText);
        Reprompt reprompt = new Reprompt();
        reprompt.setOutputSpeech(repromptOutputSpeech);
		Image image = new Image();
		image.setSmallImageUrl("https://s3.amazonaws.com/alexapwaskill1/small_logo.png");
		image.setLargeImageUrl("https://s3.amazonaws.com/alexapwaskill1/big_pay_logo.png");		 
		 StandardCard card = new StandardCard();
		 card.setTitle("Ticket Buyer");
		 card.setText(repromptText);
		 card.setImage(image);
        return SpeechletResponse.newAskResponse(outputSpeech, reprompt,card);
    }

	private SpeechletResponse tellRespose(String requestText, String respText) {
		PlainTextOutputSpeech outputSpeech = new PlainTextOutputSpeech();
		outputSpeech.setText(respText);
		Image image = new Image();
		image.setSmallImageUrl("https://s3.amazonaws.com/alexapwaskill1/small_logo.png");
		image.setLargeImageUrl("https://s3.amazonaws.com/alexapwaskill1/big_pay_logo.png");		 
		 StandardCard card = new StandardCard();
		 card.setTitle(requestText);
		 card.setText(respText);
		 card.setImage(image);
		 return SpeechletResponse.newTellResponse(outputSpeech, card);
	}
	
    private SpeechletResponse getHelp() {
		String speechOutput = "This is a test skill for ordering tickets. You can say Tickets to start the process. Now what can I help you with?";
		return newAskResponse("Help", speechOutput, speechOutput);
    }
    
    private SpeechletResponse confirmPurchase() {
		String speechOutput = "Your total is ten dollars and 88 cents." + "If you want to order the tickets now, say purchase.";
		return newAskResponse("Order", speechOutput, speechOutput);
    }
    
    private SpeechletResponse itWorks() {
 		String speechOutput = "Thanks so much, enjoy your movie.";
 		return tellRespose(speechOutput,speechOutput);
    }

    private List<Directive> buildDirectives(Session session) {
        FollowLinkTarget target = buildTarget();
        Map<String, Object> payload = buildPayload1(session);
        FollowLinkDirective followLinkDirective = new FollowLinkDirective();
        followLinkDirective.setTarget(target);
        followLinkDirective.setPayload(payload);
        // This token that will be echoed back by the Amazon Pay skill, when the result is returned.
        followLinkDirective.setToken("MERCHANT_TOKEN_OPTIONAL"); 
        try {
			System.out.println(objectMapper.writeValueAsString(followLinkDirective));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        List<Directive> directives = new ArrayList<Directive>();
        directives.add(followLinkDirective);
        return directives; 
    }
    
    private List<Directive> buildDirectivesProcessPayment(Session session) {
        FollowLinkTarget target = buildTargetProcessPayment();
        Map<String, Object> payload = buildPayload2(session);
        FollowLinkDirective followLinkDirective = new FollowLinkDirective();
        followLinkDirective.setTarget(target);
        followLinkDirective.setPayload(payload);
        // This token that will be echoed back by the Amazon Pay skill, when the result is returned.
        followLinkDirective.setToken("MERCHANT_TOKEN_OPTIONAL"); 
        try {
			System.out.println(objectMapper.writeValueAsString(followLinkDirective));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        List<Directive> directives = new ArrayList<Directive>();
        directives.add(followLinkDirective);
        return directives; 
    }
    
    private FollowLinkTarget buildTarget() {
        FollowLinkTarget followLinkTarget = new FollowLinkTarget();
        // Amazon Pay Skill Id.
        followLinkTarget.setAddress("amzn1.alexa-speechlet-client.AmazonPay"); 
        // Operation name. Value must be one of: {setup, processPayment}
        followLinkTarget.setPath("setup");
        return followLinkTarget;
     }
    
    private FollowLinkTarget buildTargetProcessPayment() {
        FollowLinkTarget followLinkTarget = new FollowLinkTarget();
        // Amazon Pay Skill Id.
        followLinkTarget.setAddress("amzn1.alexa-speechlet-client.AmazonPay"); 
        // Operation name. Value must be one of: {setup, processPayment}
        followLinkTarget.setPath("processPayment");
        return followLinkTarget;
     }
    
    private Map<String, Object> buildPayload1(Session session) {
    	System.out.println(session.getSessionId());
    	System.out.println(session.getUser());
    	String consentToken = session.getUser().getPermissions().getConsentToken();
    	System.out.println(consentToken);
    	Map<String, Object> payload = new HashMap<String, Object>();
    	payload.put("consentToken", consentToken);
    	//Seller Id required
    	payload.put("sellerId", "Your_Seller_Id");
    	payload.put("countryOfEstablishment", "US");
    	payload.put("sandboxMode", true);
    	//Sandbox account required for testing. Use an account that accepts email.
    	payload.put("sandboxCustomerEmailId", "create_sandbox_account");
        payload.put("ledgerCurrency", "USD");
        payload.put("checkoutLanguage", "en_US");
        //To suppress Amazon Pay message for first time buyer, set instantCheckout = true
        payload.put("instantCheckout", true);
        //payload.put("instantCheckout", false);

        Map<String, Object> sellerBillingAgreementAttributes = new HashMap<String, Object>();
        sellerBillingAgreementAttributes.put("sellerBillingAgreementId", "SellerSpecificId_OPTIONAL");
        sellerBillingAgreementAttributes.put("storeName", "Seller_defined_name");
        sellerBillingAgreementAttributes.put("customInformation", "Seller_defined");

        Map<String, Object> billingAgreementAttributes = new HashMap<String, Object>();
        //Platform Id typically Marketplace solution provider use case only. 
        //-Ask your Amazon Pay contact for more info.
        billingAgreementAttributes.put("platformId", "SolutionProviderId OPTIONAL");
        billingAgreementAttributes.put("sellerNote", "An Alexa order");
        billingAgreementAttributes.put("sellerBillingAgreementAttributes",  sellerBillingAgreementAttributes);
        payload.put("billingAgreementAttributes", billingAgreementAttributes);
        
        return objectMapper.convertValue(payload, Map.class);
    }
    
    private Map<String, Object> buildPayload2(Session session){
    	String consentToken = session.getUser().getPermissions().getConsentToken();
    	Map<String, Object> payloadObject = new HashMap<String, Object>();
    	payloadObject.put("consentToken", consentToken);
    	payloadObject.put("billingAgreementId", billingAgreementId);
    	payloadObject.put("sellerId", "Your_Seller_Id");
    	//See guide for paymentAction enums
    	payloadObject.put("paymentAction", "AuthorizeAndCapture" );
    	Map<String, Object> sellerOrderAttributes = new HashMap<String, Object>();
    	sellerOrderAttributes.put("sellerOrderId", "some_Id_OPTIONAL");
    	sellerOrderAttributes.put("storeName", "Ex. Alexa store");
    	sellerOrderAttributes.put("sellerNote", "sandbox_purchase");
    	sellerOrderAttributes.put("customInformation", "merchant_info_OPTIONAL");
    	Map<String, Object> authorizationAmount = new HashMap<String, Object>();
    	authorizationAmount.put("amount", "10.88");
    	authorizationAmount.put("currencyCode", "USD");
    	//AuthorizationReferenceId must be unique (required)
    	SecureRandom random = new SecureRandom();
    	Map<String, Object> authorizeAttributes = new HashMap<String, Object>();
    	authorizeAttributes.put("authorizationReferenceId", new BigInteger(128, random).toString(32));
    	authorizeAttributes.put("sellerAuthorizationNote", "some_note_OPTIONAL");
    	//the softDescriptor shows up on buyer statement
    	authorizeAttributes.put("softDescriptor", "stmnt_desc");
    	authorizeAttributes.put("authorizationAmount", authorizationAmount);
    	payloadObject.put("sellerOrderAttributes", sellerOrderAttributes);
    	payloadObject.put("authorizeAttributes", authorizeAttributes);

		return objectMapper.convertValue(payloadObject, Map.class);
    	
    }

	public SpeechletResponse onLinkResult(LinkResultRequest result, Session arg1) throws SpeechletException {
		// TODO Auto-generated method stub
		 StatusCode statusCode = result.getStatus();
		 System.out.println(result.getTarget().getPath());
		    if (StatusCode.SUCCESS.equals(statusCode)) {
		      /**
		        * Skill was able to get to a successful end state to return a successful result.
		        */
		     
		        // This payload contains the response from Amazon Pay skill. See example below
		    			//ObjectNode payload = result.getBody();
		    	parseLinkResponse(result);
		    			//For troubleshooting
		    			System.out.println(statusCode);
		    			System.out.println(result.getTarget().getPath());
		    			//End troubleshooting lines
		      if (result.getTarget().getPath().equals("setup")){
		    	  //build setup directives
		    	  return confirmPurchase();
		      }

		     } else if (StatusCode.INTERNAL_ERROR.equals(statusCode)){
		      /**
		        * Link did not return a successful response.
		        * There might be additional issues like InvalidInputParameter, etc. Check Response Payload to inspect further.
		        */
			    ObjectNode payload = result.getBody();
			    parseLinkResponse(result);
			    JsonNode errorMessage = payload.findValue("errorMessage");
			    String Code = errorMessage.toString();
		      if ("ACCESS_NOT_REQUESTED".equalsIgnoreCase(Code)) {
		             /** 
		              * If errorCode in response payload is ‘Forbidden’ then user 
		              * may not have provided Amazon Pay permission. 
		              * The following function request user to grant Amazon Pay            
		              * permission.
		              */
		              return requestAmazonPayPermission();
		         }
		     }
		      // Merchant’s business logic (User interaction with merchant)
		    //build process payment directives
		    return itWorks(); 
		  }

		  // Extract payload received from Amazon Pay skill. See the element details and sample response.
			private void parseLinkResponse(LinkResultRequest result) {
				ObjectNode payload = result.getBody();
				try {
					//for logs and troubleshooting
					System.out.println(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(result.getTarget()));
					System.out.println(result.getTarget().getPath());
				} catch (JsonProcessingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		    	 try {
		    		if	(result.getTarget().getPath().equals("processPayment")){
		    			billingAgreementId = "temp";
		    	 	 System.out.println(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(payload));
		    		 } else {
	 			     JsonNode billingAgreementDetails = payload.get("billingAgreementDetails");
	 			     billingAgreementId = billingAgreementDetails.get("billingAgreementId").asText();
	 			     System.out.println(billingAgreementId);
		    		 }
		    	 			} catch (JsonProcessingException e) {
		    	 				// TODO Auto-generated catch block
		    	 				e.printStackTrace();
		    	 			}
		 }

		 private SpeechletResponse requestAmazonPayPermission() { 
		     String speechText = "You have not yet enabled Amazon Pay for purchase. I sent you details on the Alexa app to enable Amazon Pay for this skill.";
		     AskForPermissionsConsentCard card = new AskForPermissionsConsentCard(); 
		     card.setTitle(REQUEST_AMAZON_PAY_PERMISSION_TITLE); 
		     card.setPermissions(Sets.newHashSet(AMAZON_PAY_SCOPE)); 
		     PlainTextOutputSpeech speech = new PlainTextOutputSpeech(); 
		     speech.setText(speechText); 
		     return SpeechletResponse.newTellResponse(speech, card);
		
	}
	
}

