package AmazonPay.AmazonPaySkill;

import java.util.HashSet;
import java.util.Set;

import com.amazon.speech.speechlet.lambda.SpeechletRequestStreamHandler;

public class PaySkillAppRequestStreamHandler extends SpeechletRequestStreamHandler {
	private static final Set<String> supportedApplicationIds = new HashSet<String>();
	static {
		//supportedApplicationIds.add("amzn1.ask.skill.10c31989-6ae9-4262-877a-aa9f985e74e2");
		
		supportedApplicationIds.add("amzn1.ask.skill.abdec954-528d-439b-8d58-c087d05b9e30");
		System.out.println(supportedApplicationIds);
	}
	

	public PaySkillAppRequestStreamHandler() {

		super (new PaySkillApp(), supportedApplicationIds);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
